import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { UpdatecarsdetailsComponent } from '../updatecarsdetails/updatecarsdetails.component';
import { Router } from '@angular/router';


@Component({
  selector: 'app-allcardetails',
  templateUrl: './allcardetails.component.html',
  styleUrls: ['./allcardetails.component.css']
})
export class AllcardetailsComponent implements OnInit {

  constructor(private dialog: MatDialog, private rr: Router) { }
  animal: string;
  name: string;
  carid:number;
   temparr=JSON.parse(localStorage.getItem("cardetails"));
  // temparr=JSON.parse(localStorage.getItem("cardetails"));
  radioselected:string='all';
  getAll():number
  {
   return  this.temparr.length;
  }
  getAudi():number
  {
   return  this.temparr.filter(eobj=>eobj.brandname=='Audi').length;
  }
  getMaruti():number
  {
   return  this.temparr.filter(eobj=>eobj.brandname=="Maruti").length;
  }
  delete(i:string)
  {
   // alert("hi"+i);
   if(confirm("Are you sure, you want to delete!"+i))
   {
    this.temparr.splice(i,1);
    localStorage.setItem("cardetails",JSON.stringify(this.temparr));
    this.temparr=JSON.parse(localStorage.getItem("cardetails"));
    console.log("recored Deleted!!");
   }
   else
   {
     console.log("recored not deleted");
   }

  }
  openDialog(id:number): void {
    this.carid=id;
    const dialogRef = this.dialog.open(UpdatecarsdetailsComponent
      , {
       width: '500px',
      data: {name: this.name, animal: this.animal,carid:this.carid}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  ngOnInit() {
  }
  adminLogout() {
    localStorage.removeItem('adminisLogin');
    this.rr.navigate(['/Home']);
  }
}

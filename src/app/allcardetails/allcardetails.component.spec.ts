import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllcardetailsComponent } from './allcardetails.component';

describe('AllcardetailsComponent', () => {
  let component: AllcardetailsComponent;
  let fixture: ComponentFixture<AllcardetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllcardetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllcardetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admintopbar',
  templateUrl: './admintopbar.component.html',
  styleUrls: ['./admintopbar.component.css']
})
export class AdmintopbarComponent implements OnInit {

  constructor(public rr: Router) { }

  ngOnInit() {
  }
  adminLogout() {
    localStorage.removeItem('adminisLogin');
    this.rr.navigate(['/Home']);
  }

}

import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
//import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
export interface DialogData {
  animal: string;
  name: string;
  carid:number;
  brandname:string;
}
@Component({
  selector: 'app-rentcar',
  templateUrl: './rentcar.component.html',
  styleUrls: ['./rentcar.component.css']
})
export class RentcarComponent implements OnInit {
  public rentform:FormGroup;
  constructor(
    public dialogRef: MatDialogRef<RentcarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}
    //@Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
  // this.rentform=this.formBuilder.group({
  //   carid:[this.data.carid,[Validators.required]]

  // })
  }
}
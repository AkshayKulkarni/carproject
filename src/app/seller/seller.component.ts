import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { PopupboxComponent } from './popupbox/popupbox.component';

@Component({
  selector: 'app-seller',
  templateUrl: './seller.component.html',
  styleUrls: ['./seller.component.css']
})
export class SellerComponent implements OnInit {
  constructor(public dialog: MatDialog) {}
  temparr=JSON.parse(localStorage.getItem("sellcar"));
  openDialog(): void {
    
      const dialogRef = this.dialog.open(PopupboxComponent);  

    dialogRef.afterClosed().subscribe(result => {
     
    });  
  }
  // @Component({
  //   selector: 'app-seller',
  //   templateUrl: './seller.component.html',
  // })

  ngOnInit() {
   
  }

}

import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
@Component({
  selector: 'app-popupbox',
  templateUrl: './popupbox.component.html',
  styleUrls: ['./popupbox.component.css']
})
export class PopupboxComponent implements OnInit {
  sellcarform:FormGroup;
  constructor() { }
  addcar()
  {
    var newarr=JSON.parse(localStorage.getItem("sellcar"));
    if(newarr==null)
    {
      newarr=[];
    } 
    newarr.push(this.sellcarform.value);
    localStorage.setItem("sellcar",JSON.stringify(newarr));
    console.log(newarr); 
  }

  ngOnInit() {
    this.sellcarform=new FormGroup({
      carid:new FormControl('',[Validators.required]),
      brandname:new FormControl('',[Validators.required]),
      modelname:new FormControl('',[Validators.required]),
      year:new FormControl('',[Validators.required]),
      carstate:new FormControl('',[Validators.required]),
      kmdriven:new FormControl('',[Validators.required]),
      carprice:new FormControl('',[Validators.required]),
      carimage:new FormControl('',[Validators.required])
  
    })
  }

}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  constructor() { }
  @Input()
  all:number;
  @Input()
  Audi:number;
  @Input()
  Maruti:number;
  selectedradio:String="all";
  @Output()
  childevent:EventEmitter<String>=new EventEmitter<String>();
  onSelect() {
    this.childevent.emit(this.selectedradio);
  }

  ngOnInit() {
  }

}

import { Component, OnInit } from '@angular/core';
import { trigger, transition, style, animate, animation,state } from '@angular/animations';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations:[
    trigger('move',[
      state('in',style({transform:'translateX(0)'})),
      transition('void=>left',[
        style({transform:'translateX(100%)'}),
        animate(200)
      ]),
      transition('left=>void',[
        animate(200,style({transform:'translateX(0)'}))
      ])
    ])

  ]
})
export class HomeComponent implements OnInit {
  disableSlider:boolean=false;

  constructor() { }
  public state='void';
  imageurl;
  ngOnInit() {
    this.imageurl=[
      'assets/image4.jpg',
      'assets/image5.jpg',
      'assets/image6.jpg',
      'assets/image7.jpeg',
    ];
  }
  imagerotate(arr,reverse)
    {
     if(reverse)
     {
       arr.unshift(arr.pop);
      }
      else
      arr.push(arr.shift()); 
      return arr;
    }

    moveLeft()
    {
      if(this.disableSlider)
      {
        return;  
      }
      this.state="right";
      this.imagerotate(this.imageurl,true);
    }
    onFinish($event)
    {
      this.state='void';
      this.disableSlider=false;
    }
    onStart($event)
    {
      this.disableSlider=true;
    }

}

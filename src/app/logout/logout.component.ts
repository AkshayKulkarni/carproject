import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private rr:Router) { }
            
  ngOnInit() {
    alert('Logout Successful');
    // localStorage.setItem("adminisLogin","false");
    localStorage.removeItem('adminisLogin');
    localStorage.removeItem('user');
    this.rr.navigate(['/Login']);
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminbookingdetailsComponent } from '../adminbookingdetails/adminbookingdetails.component';
import { AllcardetailsComponent } from '../allcardetails/allcardetails.component';
import {RouterModule, Routes} from '@angular/router';

// import { BooksComponent } from 'src/app/books/books.component';
// import { ViewuserComponent } from 'src/app/viewuser/viewuser.component';
import {FormsModule} from '@angular/forms';
import { AddCarComponent } from '../add-car/add-car.component';
import { ChildComponent } from '../child/child.component';
import { UpdateCarsComponent } from '../update-cars/update-cars.component';
import { UpdatecarsdetailsComponent } from '../updatecarsdetails/updatecarsdetails.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule, MatButtonModule } from '@angular/material';
// import{ ChildComponent} from 'src/app/books/child/child.component';
import { LogoutComponent } from '../logout/logout.component';
import { HomeComponent } from '../home/home.component';
import { AdmintopbarComponent } from '../admintopbar/admintopbar.component';

const routes: Routes = [
{path: 'adminsidebooking', component: AdminbookingdetailsComponent},
{path: 'Addcar', component: AddCarComponent},
{path: 'Home', component: HomeComponent},
{path: 'AllCardetails', component: AllcardetailsComponent},
{path: 'UpdateCars/:c', component: UpdateCarsComponent},
{path: 'logout', component: LogoutComponent}
];

@NgModule({
  declarations: [AdminbookingdetailsComponent, AddCarComponent, AllcardetailsComponent,
    ChildComponent, UpdateCarsComponent, UpdatecarsdetailsComponent, AdmintopbarComponent],
  imports: [RouterModule.forChild(routes), CommonModule, FormsModule, MatDialogModule, MatFormFieldModule, MatButtonModule],
  entryComponents: [UpdatecarsdetailsComponent],
  exports: [RouterModule]
})
export class AdminModule { }

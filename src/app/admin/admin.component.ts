import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private rr: Router) { }

  ngOnInit() {
  }
  adminLogout() {
    localStorage.removeItem('adminisLogin');
    this.rr.navigate(['/Home']);
  }
}

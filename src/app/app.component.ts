import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'carproject';
  isLoggedIn() {
  if (localStorage.getItem('user')) {
    return true;
  }
    return false;
  }
  isAdminLoggedIn() {
    if (localStorage.getItem('adminisLogin')) {
      return true;
    }
    return false;
  }
}


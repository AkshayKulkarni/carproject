import { Component, OnInit } from '@angular/core';
import { CarService } from '../car.service';

@Component({
  selector: 'app-customerhistory',
  templateUrl: './customerhistory.component.html',
  styleUrls: ['./customerhistory.component.css']
})
export class CustomerhistoryComponent implements OnInit {

  constructor(private esr:CarService) { }
  histroydata:any[];
  userarr:any;
  ngOnInit() {
    this.userarr=JSON.parse(localStorage.getItem("user"));
    this.histroydata=this.esr.userhistorydetails(this.userarr.emailid);
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { BuyerComponent } from './buyer/buyer.component';
import { SellerComponent } from './seller/seller.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { LoginComponent } from './login/login.component';
import {Routes, RouterModule} from "@angular/router";
import { RegisterComponent } from './register/register.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CarService } from './car.service';
import { DemoMaterialModule } from './material/material.module';
import { PopupboxComponent } from './seller/popupbox/popupbox.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AdminsellerComponent } from './adminseller/adminseller.component';
import { AdminrentComponent } from './adminrent/adminrent.component';
import { AdminComponent } from './admin/admin.component';
import { AddrentdetailsComponent } from './adminrent/addrentdetails/addrentdetails.component';
import { RentcarComponent } from './buyer/rentcar/rentcar.component';
import { RentcComponent } from './rentc/rentc.component';
import { ExampleComponent } from './example/example.component';
import { BookingComponent } from './booking/booking.component';
import { CarsreaddComponent } from './carsreadd/carsreadd.component';
//import { AddCarComponent } from './add-car/add-car.component';
//import { AllcardetailsComponent } from './allcardetails/allcardetails.component';
//import { UpdateCarsComponent } from './update-cars/update-cars.component';
//import { ChildComponent } from './child/child.component';
import { BillComponent } from './bill/bill.component';
import { AuthGuard } from './auth.guard';
import { LogoutComponent } from './logout/logout.component';
import { AdminbookingdetailsComponent } from './adminbookingdetails/adminbookingdetails.component';
import { CustomerhistoryComponent } from './customerhistory/customerhistory.component';
import {AdminModule} from './admin/admin.module';
import {SliderModule} from 'angular-image-slider';
import { UsertopbarComponent } from './usertopbar/usertopbar.component';
// import { AdmintopbarComponent } from './admintopbar/admintopbar.component';

// import { UpdatecarsdetailsComponent } from './updatecarsdetails/updatecarsdetails.component';
const routes: Routes = [
  {path: '', redirectTo: '/Home', pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'Home', component: HomeComponent, canActivate: [AuthGuard]},
  // {path:'Buyer',component:BuyerComponent},
  // {path:'Seller',component:SellerComponent},
  // {path:'adminsidebooking',component:AdminbookingdetailsComponent},
  {path: 'Login', component: LoginComponent},
  {path: 'Register', component: RegisterComponent},
  {path: 'admin', component: AdminComponent},
  {path: 'adminsell', component: AdminsellerComponent},
  {path: 'adminrent', component: AdminrentComponent},
  {path: 'Rentcar', component: RentcarComponent},
  {path: 'customerhistory', component: CustomerhistoryComponent, canActivate: [AuthGuard]},
  {path: 'Rentc', component: RentcComponent, canActivate: [AuthGuard]},
  {path: 'Example/:a', component: ExampleComponent},
  {path: 'Booking', component: BookingComponent, canActivate: [AuthGuard]},
  {path: 'Carsreadd/:b', component: CarsreaddComponent},
  // {path:'UpdateCars/:c',component:UpdateCarsComponent},
 // {path:'Addcar',component:AddCarComponent,canActivate:[AuthGuard]},
 //  {path:'AllCardetails',component:AllcardetailsComponent,canActivate:[AuthGuard]},
  {path: 'bill/:d', component: BillComponent},
  {path: 'Logout', component: LogoutComponent, canActivate: [AuthGuard]},
  {path: '**', component: PagenotfoundComponent}
  ];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutusComponent,
    BuyerComponent,
    SellerComponent,
    PagenotfoundComponent,
    LoginComponent,
    RegisterComponent,
    PopupboxComponent,
    AdminsellerComponent,
    AdminrentComponent,
    AdminComponent,
    AddrentdetailsComponent,
    RentcarComponent,
    RentcComponent,
    ExampleComponent,
    BookingComponent,
    CarsreaddComponent,
   // AddCarComponent,
  //   AllcardetailsComponent,
    // UpdateCarsComponent,
    BillComponent,
    LogoutComponent,
    // AdminbookingdetailsComponent,
    CustomerhistoryComponent,
    UsertopbarComponent,
    // AdmintopbarComponent,
  ],
  entryComponents: [PopupboxComponent, AddrentdetailsComponent, RentcarComponent],
  imports: [
    BrowserModule, RouterModule.forRoot(routes),
    BrowserAnimationsModule, ReactiveFormsModule, FormsModule, DemoMaterialModule, AdminModule,SliderModule
  ],
  providers: [CarService],
  bootstrap: [AppComponent]
})
export class AppModule { }

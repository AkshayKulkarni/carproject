import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CarService } from '../car.service';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {

  constructor(private act: ActivatedRoute, private esr: CarService) { }
  code: number;
  obj: {};
  billdata: any[];
  userarr: any;
  // temparr;
  temparr = JSON.parse(localStorage.getItem('custinfo'));
 // temparr=JSON.parse(localStorage.getItem("cardetails"));
  carsreadd(id: number) {
  //  alert("id is"+id);
  }
  ngOnInit() {
    // this.code=parseInt(this.act.snapshot.paramMap.get('b'));
    // alert("code is "+this.code);
    // this.obj=this.esr.Upaterentdetails(this.code);
     this.userarr = JSON.parse(localStorage.getItem('user'));
    // this.userarr=JSON.parse(localStorage.getItem("userifo"));
     this.billdata = this.esr.userbilldetails(this.userarr.emailid);
  }

}

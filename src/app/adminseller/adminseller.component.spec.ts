import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminsellerComponent } from './adminseller.component';

describe('AdminsellerComponent', () => {
  let component: AdminsellerComponent;
  let fixture: ComponentFixture<AdminsellerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminsellerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminsellerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

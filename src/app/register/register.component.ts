import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  userform:FormGroup;
  constructor( private router:Router) { }
  data:{};
  save()
  {
  //  this.data={};
    var newarr=JSON.parse(localStorage.getItem("userinfo"));
    if(newarr==null)
    {
      newarr=[];
    } 
    newarr.push(this.userform.value);
    localStorage.setItem("userinfo",JSON.stringify(newarr));
    console.log(newarr);
    //this.emp=JSON.parse(localStorage.getItem("userinfo"));
    alert("Register Succeessfully");
    this.router.navigate(['/Login']);
  }

  ngOnInit() {
    this.userform = new FormGroup({
      name: new FormControl('', [Validators.required]),
      emailid: new FormControl('', [Validators.required, Validators.email,
      Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
      gender: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
      country: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)])
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CarService } from '../car.service';

@Component({
  selector: 'app-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.css']
})
export class BillComponent implements OnInit {
  code;
  data1:{};
  obj:number;
  actulacarsdetails;
  constructor(private act: ActivatedRoute, private esr: CarService, private rr: Router) { }

  ngOnInit() {
    this.code = parseInt(this.act.snapshot.paramMap.get('d'));
    // alert("code"+this.code);
    this.data1 = this.esr.custDetails(this.code);
    this.obj = this.esr.billgenderated(this.code);
  }
  temparr=JSON.parse(localStorage.getItem('custinfo'));
  pay(id) {
    var newarr=JSON.parse(localStorage.getItem('billdetails'));
    if (newarr ==null) {
      newarr = [];
    }
    newarr.push(this.data1);
    localStorage.setItem('billdetails', JSON.stringify(newarr));
    // console.log(newarr);
    alert('added successfully');
    //this.rr.navigate(['/Home']);
    for(var i=0;i<this.temparr.length;i++) {
        if (this.temparr[i].carid==id) {
          // alert("hi"+i);
          this.temparr.splice(i,1);
          localStorage.setItem('custinfo', JSON.stringify(this.temparr));
          this.temparr = JSON.parse(localStorage.getItem('custinfo'));
          // alert("deleted successfully");
          // this.rr.navigate(['/Home']);
        }
    }

    this.actulacarsdetails = JSON.parse(localStorage.getItem('cardetails'));
    for(var i=0;i<this.actulacarsdetails.length;i++) { 
      if (this.actulacarsdetails[i].carid==id) {
          // alert(id+"done");
          this.actulacarsdetails[i].flag = '0';
          break;
          // return this.actulacarsdetails[i];
        }
    }
    localStorage.setItem('cardetails', JSON.stringify(this.actulacarsdetails));
    localStorage.removeItem('isbooking');
    this.rr.navigate(['/Home']);
    // localStorage.setItem("custinfo",JSON.stringify(this.temparr));
    // this.temparr=JSON.parse(localStorage.getItem("custinfo"));
  }

}

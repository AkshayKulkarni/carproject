import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-car',
  templateUrl: './add-car.component.html',
  styleUrls: ['./add-car.component.css']
})
export class AddCarComponent implements OnInit {

  constructor(private rr: Router) { }
 // constructor() {flag=0 }
// rentcaradd:FormGroup;
  // temparr:[];
  model:any={};
  //carimage;
  selectedFiles:File=null;
    onFileSelected(File:FileList)
    {
      this.selectedFiles = File.item(0);
      var reader = new FileReader();
      reader.onload = (event: any)=>{
        this.model.carimage = event.target.result;
      }
      reader.readAsDataURL(this.selectedFiles);
    }
  addcar() {
    var newarr=JSON.parse(localStorage.getItem("cardetails"));
    if(newarr==null)
    {
      newarr=[];
    }
    //newarr.push(this.rentcaradd.value);
    newarr.push(this.model);
    localStorage.setItem("cardetails",JSON.stringify(newarr));
    localStorage.setItem("Actualcaretails",JSON.stringify(newarr));
    alert("Added Successfully");
    console.log(newarr); 
   // newarr="";
  }
  adminLogout() {
    localStorage.removeItem('adminisLogin');
    this.rr.navigate(['/Home']);
  }
  
  ngOnInit() {
    //flag=0;
   // this.temparr=JSON.parse(localStorage.getItem("cardetails"));
    // this.rentcaradd=new FormGroup({
    //   carid:new FormControl('',[Validators.required]),
    //   brandname:new FormControl('',[Validators.required]),
    //   modelname:new FormControl('',[Validators.required]),
    //   type:new FormControl('',[Validators.required]),
    //   seaters:new FormControl('',[Validators.required]),
    //   // todate:new FormControl('',[Validators.required]),
    //   // fromdate:new FormControl('',[Validators.required]),
    //   // driver:new FormControl('',[Validators.required]),
    //   perkmrate:new FormControl('',[Validators.required]),
    //   carimage:new FormControl('',[Validators.required])
    // })
    
  }

}

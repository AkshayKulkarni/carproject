import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddrentdetailsComponent } from './addrentdetails.component';

describe('AddrentdetailsComponent', () => {
  let component: AddrentdetailsComponent;
  let fixture: ComponentFixture<AddrentdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddrentdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddrentdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminrentComponent } from './adminrent.component';

describe('AdminrentComponent', () => {
  let component: AdminrentComponent;
  let fixture: ComponentFixture<AdminrentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminrentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminrentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

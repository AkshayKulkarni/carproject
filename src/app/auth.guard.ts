import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree,CanActivate} from '@angular/router';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements  CanActivate {
  
  canActivate():boolean
  {
    let Log=localStorage.getItem("adminisLogin");
    if(Log==="true")
    {
      return true;
    }
    else
    return false;
  }
  // canActivate2():boolean
  // {
  //   let Log=localStorage.getItem("adminisLogin1");
  //   if(Log==="true")
  //   {
  //     return true;
  //   }
  //   else
  //   return false;
  // }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatecarsdetailsComponent } from './updatecarsdetails.component';

describe('UpdatecarsdetailsComponent', () => {
  let component: UpdatecarsdetailsComponent;
  let fixture: ComponentFixture<UpdatecarsdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatecarsdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatecarsdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

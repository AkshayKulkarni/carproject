import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CarService } from '../car.service';
export interface DialogData {
  animal: string;
  name: string;
  carid:number;
}
@Component({
  selector: 'app-updatecarsdetails',
  templateUrl: './updatecarsdetails.component.html',
  styleUrls: ['./updatecarsdetails.component.css']
})
export class UpdatecarsdetailsComponent implements OnInit {
  code: any;
  obj:{};
  acc: any;
  // constructor() { }
  constructor(private esr:CarService,public dialogRef: MatDialogRef<UpdatecarsdetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }
    
    onNoClick(): void {
      this.dialogRef.close();
    }

  ngOnInit() {
    this.code=this.data.carid;
    this.obj=this.esr.getCarsbyId(this.code);
  }
  updatecars()
  {
    this.acc=JSON.parse(localStorage.getItem("cardetails"));
    for(let i=0;i<this.acc.length;i++)
    {
      if(this.acc[i].carid==this.code)
      {
        this.acc[i]=this.obj;
        localStorage.setItem("cardetails",JSON.stringify(this.acc));
        alert("update Successfully");
        //this.router.navigate(['/Home']);  
        
        window.location.reload();
        //this.dialogRef.close();
      }
    }
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RentcComponent } from './rentc.component';

describe('RentcComponent', () => {
  let component: RentcComponent;
  let fixture: ComponentFixture<RentcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RentcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RentcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

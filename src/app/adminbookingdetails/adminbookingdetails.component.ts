import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-adminbookingdetails',
  templateUrl: './adminbookingdetails.component.html',
  styleUrls: ['./adminbookingdetails.component.css']
})
export class AdminbookingdetailsComponent implements OnInit {
  constructor() { }
  temparr=JSON.parse(localStorage.getItem("custinfo"));
  adminlogin() {
    if (localStorage.getItem('adminisLogin')) {
      return true;
    }
    return false;
  }
  ngOnInit() {
  }

}

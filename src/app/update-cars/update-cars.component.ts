import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CarService } from '../car.service';

@Component({
  selector: 'app-update-cars',
  templateUrl: './update-cars.component.html',
  styleUrls: ['./update-cars.component.css']
})
export class UpdateCarsComponent implements OnInit {
  code:number;
  obj:any={};
  acc:any[]=[];
  data1:any={};
  constructor(private act:ActivatedRoute,private esr:CarService,private router:Router) { }
  
  ngOnInit() {
    this.acc=JSON.parse(localStorage.getItem("cardetails"));
    this.code=parseInt(this.act.snapshot.paramMap.get('c'));
    alert("code is "+this.code);
    this.obj=this.esr.getCarsbyId(this.code);
  }

  updatecars()
  {
    this.acc=JSON.parse(localStorage.getItem("cardetails"));
    for(let i=0;i<this.acc.length;i++)
    {
      if(this.acc[i].carid==this.code)
      {
        this.acc[i]=this.obj;
        localStorage.setItem("cardetails",JSON.stringify(this.acc));
        alert("update Successfully");
        this.router.navigate(['/AllCardetails']);  
      }
    }
  }


}

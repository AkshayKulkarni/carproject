import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarsreaddComponent } from './carsreadd.component';

describe('CarsreaddComponent', () => {
  let component: CarsreaddComponent;
  let fixture: ComponentFixture<CarsreaddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarsreaddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarsreaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
